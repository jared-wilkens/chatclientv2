#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <sys/select.h>
#include <sys/time.h>
#include <pthread.h>
#include <ncurses.h>

/*
Author: Jared Wilkens on February 25th 2019

I got help from:
John Rodkey, used the base code he provided for this project
Matthew Coffman, we fiqured out multi-threading together
Matthew Miller, helped explained ncurses 
Ryan Kleinberg, worked on ncurses and general code structure together
Christan Alvo, worked on ncurses and general code structure together
James, learned about ncurses functions through him
Sophia, talked about ncurses and how it works 
Dr. Patterson, suggested we use mulit-threading in project one 
https://www.geeksforgeeks.org/multithreading-c-2/, multi-threading help
http://www.amparo.net/ce155/thread-ex.html, mulit-threading help
https://www.youtube.com/watch?v=pjT5wq11ZSE, ncurses help

I gave help to:
Matthew Coffman, we fiqured out multi-threading together, explained ncurses
Ryan Kleinberg, explained ncurses, multi-threading, and general code structure  
Matthew Miller, explained multi-threading
Christan Alvo, explained multi-threading, worked on ncurses and general code structure together
*/

//creates constants for later use such as server ip, port number, and message buffer size
#define SERVER "10.115.20.250"
#define PORT 49153
#define BUFSIZE 1024

//prototypes functions 
int connect2v4stream(char* , int);
int sendout(int, char *);
void recvandprint (int, char*);
void* write_fun(void*);
void* read_fun(void*);
void create_win();

//global declaration of variables used by functions in different threads and locations
fd_set read_fds;
struct timeval timev;
int fd, len;

//global declaration of windows for ncurses
WINDOW* rec_win;
WINDOW* rec_wr;
WINDOW* write_win;
WINDOW* write_wr;


//main function that runs program
int main(int argc, char * argv[]){
    
    //creates buffers that will be used for sending a username and messages
    char *name, *buffer, *origbuffer;

    //declares two threads, a read and a write thread they can run at the same time
    // allowing message sending and receiving to occur at the same time
    pthread_t read_thread, write_thread;
    
    //calls function that creates windows for ncurses formatting
    create_win();
    
    //calls function that connects client to server
    fd = connect2v4stream(SERVER, PORT);
    
    //Setup recv timeout for .5 seconds
    timev.tv_sec = 0;
    timev.tv_usec = 1000 * 500;
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));
    
    //set name based on arguments and exits with an error if one is encountered
    if(argc < 2){
        printf("Usage: chat-cleint <screenname>\n");
        exit(1);
    }
    
    //appends formating and sends username
    name = argv[1];
    len = strlen(name);
    name[len] = '\n'; 
    name[len+1] = '\0';
    sendout(fd, name);
    
    //creates two threads, a read thread which takes incoming messages from the
    //server and a write thread which sends messages to the server. The threads 
    //require a function to be created a read function and write function are each
    //defined for that purpose later in the program
    pthread_create(&read_thread, NULL, read_fun, NULL);
    pthread_create(&write_thread, NULL, write_fun, NULL);
    
    //starts the threads that were just created both threads run at the same time
    pthread_join(read_thread, NULL);
    pthread_join(write_thread, NULL);
    
    return 0;
}


//creates ncurses windows that are printed to in order that the program may be 
//formatted well
void create_win(){
    
    //creates screen and defines formatting values that are used later to place
    // and formatt windows 
    initscr();
    int start_y, start_x, height, width;
    int max_h, max_w;
    getmaxyx(stdscr, max_h, max_w);
    height = max_h/2;
    width = max_w;
    start_y = 1;
    start_x = 0;
    
    //creates a window with a border on the top half of the screen and refreshes
    rec_win = newwin(height, width, start_y, start_x);
    refresh();
    wrefresh(rec_win);
    box(rec_win, 0, 0);
    
    //creates a window inside the rec_win box where the received messages 
    //will be printed later in the program
    rec_wr = newwin(height-2, width-2, start_y+1, start_x+1);
    refresh();
    wrefresh(rec_wr);
    scrollok(rec_wr, TRUE);
    
    //creates a window with a border on the bottom half of the screen
    write_win = newwin(height -1, width, height +2, start_x);
    refresh();
    wrefresh(write_win);
    box(write_win,0,0);
    wrefresh(write_win);
    
    //creates a window inisde the write_win box where the outgoing messages 
    //will placed later in the program
    write_wr = newwin(height -3, width -2, height+3, start_x+1);
    keypad(write_wr, true);
    wmove(write_wr, 0,1);
    refresh();
    wrefresh(write_win);
    scrollok(write_wr, TRUE);
    
    refresh();
}


//the write function that is passed to the write_thread, the function sends
//messages to the server once the thread is joined
void* write_fun(void *place_holder){
    
    char *buffer;
    char *origbuffer;
    int len;
    int is_done = 0;
    
    while(! is_done){
        
        len = BUFSIZE;
        buffer = malloc(len+1);
        origbuffer = buffer;
        
        wgetstr(write_wr, buffer);
        strcat(buffer, "\n");
        origbuffer = buffer;
        
        sendout(fd,buffer);
        is_done = (strcmp (buffer, "quit\n") == 0);
        free(origbuffer);
    }
    
    return NULL;
}


//the read function that is passed to the read_thread, the function reads
//messages from teh server and prints thems
void* read_fun(void *place_holder){
    
    while(1){
        
        char *buff_holder;
        recvandprint(fd, buff_holder);
    }
    
    return NULL;
}


//connects cilent to the server
int connect2v4stream(char * srv, int port){
    
    int ret, sockd;
    struct sockaddr_in sin;
    
    if((sockd = socket(AF_INET, SOCK_STREAM,0)) == -1){
        printf("ERROR: Error creating socket. errno = %d\n", errno);
        exit(errno);
    }
    
    if ((ret = inet_pton(AF_INET, SERVER, &sin.sin_addr)) <= 0){
        printf("ERROR: trouble converting using inet_pton. \
                return balue = %d, errno = %d\n", ret, errno);
                exit(errno);
    }

    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);
    
    if((connect(sockd, (struct sockaddr *) &sin, sizeof(sin))) == -1){
        printf("ERROR: trouble connecting to server. errno = %d\n", errno);
        exit(errno);
    }
    
    return sockd;
}


//sends messages to the server is called by the write_fun 
int sendout( int fd, char *msg){
    
    int ret;
    
    if(strlen(msg) > 1){
        ret=send(fd, msg, strlen(msg), 0);
    }
    
    if (ret == -1){
        printf("ERROR: trouble sending. errno = %d\n", errno);
        exit(errno);
    }
    
    return strlen(msg);
}


//reads and prints messages from the server called by the read_fun 
//in the read thread
void recvandprint (int fd, char *buff ){
    
    int ret;
    
    for(;;){
        
        buff = malloc(BUFSIZE+1);
        ret = recv(fd,buff,BUFSIZE,0);
        if(ret==-1){
            if(errno == EAGAIN){
                break; // do it again
            } else{
                printf("ERROR: error receiving. errno = %d\n", errno);
                exit(errno);
            }
        }else if (ret == 0){
            exit(0);
        } else {
            buff[ret] = 0;
            wprintw(rec_wr, buff);
            wrefresh(rec_wr);
            wrefresh(rec_win);
        }
        
        free(buff);
    }
}